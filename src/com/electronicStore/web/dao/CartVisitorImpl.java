package com.electronicStore.web.dao;

import java.math.BigDecimal;

public class CartVisitorImpl implements CartVisitor {
	
	public BigDecimal visit(CartItem cartItem) {
		BigDecimal cost, discount;
		cost = new BigDecimal("0.00");
		discount = new BigDecimal("0.00");
		if(cartItem.getTotalPrice().compareTo(new BigDecimal("150.00")) == 1){
			discount = cartItem.getTotalPrice().multiply(new BigDecimal("0.10"));
			cost = cartItem.getTotalPrice().subtract(discount);
			cartItem.setDiscount(discount);
			cartItem.setCost(cost);
		}else if(cartItem.getTotalPrice().compareTo(new BigDecimal("50.00")) == 1){
			discount = cartItem.getTotalPrice().multiply(new BigDecimal("0.05"));
			cost = cartItem.getTotalPrice().subtract(discount);
			cartItem.setDiscount(discount);
			cartItem.setCost(cost);
		}else if(cartItem.getTotalPrice().compareTo(new BigDecimal("450.00")) == 1){
			discount = cartItem.getTotalPrice().multiply(new BigDecimal("0.15"));
			cost = cartItem.getTotalPrice().subtract(discount);
			cartItem.setDiscount(discount);
			cartItem.setCost(cost);
		}else cost = cartItem.getTotalPrice();
	
		return cost;
	}
}