package com.electronicStore.web.dao;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
@Component("cartDao")
public class CartDao {

	@Autowired
	private SessionFactory sessionFactory;

	public Session session() {
		return sessionFactory.getCurrentSession();
	}

	public void create(Cart cart) {
		session().save(cart);
	}
	public void update(Cart cart) {
		session().update(cart);
	}

	public boolean exists(int cartid) {
		Cart cart = getCartById(cartid);
		return cartid != 0;
		
	}

	@SuppressWarnings("unchecked")
	public List<Cart> getAllCarts() {
		return session().createQuery("from Cart").list();
	}

	public Cart getCartById(int cartId) {
		Criteria criteria = session().createCriteria(Cart.class);
		criteria.add(Restrictions.eq("cartId", cartId));
		return (Cart)criteria.uniqueResult();
		
	}

}
