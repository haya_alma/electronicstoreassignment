package com.electronicStore.web.dao;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table
public class Product implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "productId")
	private int productId;
	private String productTitle;
	private String productCategory;
	private String productDescription;
	private BigDecimal productPrice;
	private String productStatus;
	private int unitInStock;
	private String productManufacturer;
	private String productImage;

	public Product() {
	}

	public Product(String productTitle, String productCategory, String productDescription, BigDecimal productPrice,
			String productStatus, int unitInStock, String productManufacturer, String productImage) {
		this.productTitle = productTitle;
		this.productCategory = productCategory;
		this.productDescription = productDescription;
		this.productPrice = productPrice;
		this.productStatus = productStatus;
		this.unitInStock = unitInStock;
		this.productManufacturer = productManufacturer;
		this.productImage = productImage;
	}

	public Product(String productTitle, String productCategory) {
		this.productTitle = productTitle;
		this.productCategory = productCategory;
	}

	public String getProductManufacturer() {
		return productManufacturer;
	}

	public void setProductManufacturer(String productManufacturer) {
		this.productManufacturer = productManufacturer;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productTitle;
	}

	public void setProductName(String productName) {
		this.productTitle = productName;
	}

	public String getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public BigDecimal getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(BigDecimal productPrice) {
		productPrice.setScale(2, BigDecimal.ROUND_DOWN);
		this.productPrice = productPrice;
	}

	public String getProductStatus() {
		return productStatus;
	}

	public void setProductStatus(String productStatus) {
		this.productStatus = productStatus;
	}

	public int getUnitInStock() {
		return unitInStock;
	}

	public void setUnitInStock(int unitInStock) {
		this.unitInStock = unitInStock;
	}

	public String getProductImage() {
		return productImage;
	}

	public void setProductImage(String productImage2) {
		this.productImage = productImage2;
	}

	@Override
	public String toString() {
		return "Product [productId=" + productId + ", productTitle=" + productTitle + ", productCategory="
				+ productCategory + ", productDescription=" + productDescription + ", productPrice=" + productPrice
				+ ", productStatus=" + productStatus + ", unitInStock=" + unitInStock + ", productManufacturer="
				+ productManufacturer + ", productImage=" + productImage + "]";
	}


}
