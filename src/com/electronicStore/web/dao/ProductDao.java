package com.electronicStore.web.dao;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
@Component("productDao")
public class ProductDao {

	@Autowired
	private SessionFactory sessionFactory;

	public Session session() {
		return sessionFactory.getCurrentSession();
	}

	@Transactional()
	public void saveOrUpdate(Product product) {
		session().saveOrUpdate(product);

	}
	@SuppressWarnings("unchecked")
	public List<Product> getAllProducts() {
		return session().createQuery("from Product").list();

	}
	public Product getProduct(int productid) {
		Criteria criteria = session().createCriteria(Product.class);
		criteria.add(Restrictions.idEq(productid));
		return (Product)criteria.uniqueResult();
	}
	
}
