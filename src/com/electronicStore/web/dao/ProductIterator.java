package com.electronicStore.web.dao;

public interface ProductIterator {

	public boolean hasNext();
	
	public boolean hasNextM();
	
	public Product next();
}