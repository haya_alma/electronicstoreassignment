package com.electronicStore.web.dao;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

	
@Entity
@Table(name="BillingAddress")
public class BillingAddress  implements Serializable{
		
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name="id")
		private int id;
		private String country;
		private String city;
		private String addr1;
		private String addr2;
		private String addr3;
		
		@OneToOne
		@JoinColumn(name = "orderId")
		private Orders order;
		
		public BillingAddress(){}
		
		public BillingAddress(String country, String city, String addr1, String addr2, String addr3, Orders order) {
			this.country = country;
			this.city = city;
			this.addr1 = addr1;
			this.addr2 = addr2;
			this.addr3 = addr3;
			this.order=order;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getCountry() {
			return country;
		}

		public void setCountry(String country) {
			this.country = country;
		}

		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}

		public String getAddr1() {
			return addr1;
		}

		public void setAddr1(String addr1) {
			this.addr1 = addr1;
		}

		public String getAddr2() {
			return addr2;
		}

		public void setAddr2(String addr2) {
			this.addr2 = addr2;
		}

		public String getAddr3() {
			return addr3;
		}

		public void setAddr3(String addr3) {
			this.addr3 = addr3;
		}

		public Orders getOrder() {
			return order;
		}

		public void setOrder(Orders order) {
			this.order = order;
		}

		@Override
		public String toString() {
			return "BillingAddress [id=" + id + ", country=" + country + ", city=" + city + ", addr1=" + addr1
					+ ", addr2=" + addr2 + ", addr3=" + addr3 + "]";
		}
		
		
		
		
}
