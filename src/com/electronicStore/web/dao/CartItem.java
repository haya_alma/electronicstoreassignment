package com.electronicStore.web.dao;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "CartItem")
public class CartItem implements Serializable, CartItemI {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cartItemId")
	private int cartItemId;
	private int quantity;
	private BigDecimal totalPrice;
	private BigDecimal cost;
	private BigDecimal discount;

	@ManyToOne
	@JoinColumn(name = "cartId")
	private Cart cart;

	@ManyToOne
	@JoinColumn(name = "productId")
	private Product product;

	public CartItem() {
	}
	public CartItem(int quantity, BigDecimal totalPrice, BigDecimal cost, BigDecimal discount, Cart cart,
			Product product) {
		this.quantity = quantity;
		this.totalPrice = totalPrice;
		this.cost = cost;
		this.discount = discount;
		this.cart = cart;
		this.product = product;
	}
	@Override
	public BigDecimal accept(CartVisitor visitor) {
		return visitor.visit(this);
	}
	
	public int getCartItemId() {
		return cartItemId;
	}

	public void setCartItemId(int cartItemId) {
		this.cartItemId = cartItemId;
	}

	public Cart getCart() {
		return cart;
	}

	public void setCart(Cart cart) {
		this.cart = cart;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public BigDecimal getCost() {
		return cost;
	}
	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	@Override
	public String toString() {
		return "CartItem [cartItemId=" + cartItemId + ", quantity=" + quantity + ", totalPrice=" + totalPrice
				+ ", cost=" + cost + ", discount=" + discount + ", cart=" + cart + ", product=" + product + "]";
	}
	

	

}
