package com.electronicStore.web.dao;

import java.math.BigDecimal;

public interface CartItemI {
	public BigDecimal accept(CartVisitor visitor);
}