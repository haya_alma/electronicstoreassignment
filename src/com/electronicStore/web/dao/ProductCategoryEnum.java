package com.electronicStore.web.dao;

public enum ProductCategoryEnum {
	LAPTOP, TABLET, TV, AUDIO, ALL;
}