package com.electronicStore.web.dao;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Entity
@Table(name="orders")
public class Orders implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
    private int id;
	private String paymentType;
    private BigDecimal paidAmount;
	private Timestamp date;
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "userId")
    private User user;
	
	public Orders(){}
	
	public Orders(String paymentType, BigDecimal paidAmount, Timestamp date, User user) {
		this.paymentType = paymentType;
		this.paidAmount = paidAmount;
		this.date = date;
		this.user = user;
	} 
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public BigDecimal getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Orders [id=" + id + ", paymentType=" + paymentType + ", paidAmount=" + paidAmount + ", date=" + date
				+ "]";
	}

}
