package com.electronicStore.web.dao;

import java.util.List;

public class ProductIteratorImpl implements ProductIterator {

	private ProductCategoryEnum type;
	private ProductManufacturerEnum productManufacturer;
	private List<Product> products;
	private int position;

	public ProductIteratorImpl(ProductCategoryEnum type, List<Product> productsList) {
		this.type = type;
		this.products = productsList;
	}
	public ProductIteratorImpl(ProductManufacturerEnum pm, List<Product> productsList) {
		this.productManufacturer = pm;
		this.products = productsList;
	}

	@Override
	public boolean hasNext() {
		while (position < products.size()) {
			Product product = products.get(position);
			if (ProductCategoryEnum.valueOf(product.getProductCategory()).equals(type) || type.equals(ProductCategoryEnum.ALL)) {
				return true;
			} else
				position++;
		}
		return false;
	}
	
	public boolean hasNextM() {
		while (position < products.size()) {
			Product product = products.get(position);
			if (ProductManufacturerEnum.valueOf(product.getProductManufacturer()).equals(productManufacturer) || productManufacturer.equals(ProductManufacturerEnum.ALL)) {
				return true;
			} else
				position++;
		}
		return false;
	}

	@Override
	public Product next() {
		Product product= products.get(position);
		position++;
		return product;
	}

}