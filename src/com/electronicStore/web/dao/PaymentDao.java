package com.electronicStore.web.dao;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
@Component("paymentDao")
public class PaymentDao {

	@Autowired
	private SessionFactory sessionFactory;

	public Session session() {
		return sessionFactory.getCurrentSession();
	}

	public void saveCard(CreditCardStrategy cardStrategy) {
		session().save(cardStrategy);
	}
	@SuppressWarnings("unchecked")
	public List<CreditCardStrategy> getAllCreditCardStrategy() {
		return session().createQuery("from CreditCardStrategy").list();
	}
	
	public void savePaypal(PaypalStrategy paypalStrategy) {
		session().save(paypalStrategy);
	}
	@SuppressWarnings("unchecked")
	public List<PaypalStrategy> getAllPaypalStrategy() {
		return session().createQuery("from PaypalStrategy").list();
	}
	

}
