package com.electronicStore.web.dao;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
@Component("commentRateDao")
public class CommentRateDao {
	@Autowired
	private SessionFactory sessionFactory;

	public Session session() {
		return sessionFactory.getCurrentSession();
	}
	@Transactional()
	public void saveOrUpdate(Comment comment) {
		session().saveOrUpdate(comment);
	}
	@Transactional()
	public void delete(Comment comment) {
		session().delete(comment);
	}
	@SuppressWarnings("unchecked")
	public List<Comment> allComments( ) {
		return session().createCriteria(Comment.class).list();
	}
	@Transactional()
	public void saveOrUpdate(Rate rate) {
		session().saveOrUpdate(rate);
	}
	@SuppressWarnings("unchecked")
	public List<Rate> allRates( ) {
		return session().createCriteria(Rate.class).list();
	}
}
