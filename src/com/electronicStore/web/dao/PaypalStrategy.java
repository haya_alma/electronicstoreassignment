package com.electronicStore.web.dao;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PaypalStrategy")
public class PaypalStrategy implements PaymentStrategy, Serializable {
	   
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	private String email;
	
	public PaypalStrategy(String email){
		this.email=email;
	}
	
	@Override
	public void pay(BigDecimal amount) {
		System.out.println(amount + " paid using Paypal.");
	}

}
