package com.electronicStore.web.dao;

import java.math.BigDecimal;

public interface CartVisitor {
	BigDecimal visit(CartItem product);
}