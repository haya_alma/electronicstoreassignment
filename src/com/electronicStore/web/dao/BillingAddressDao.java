package com.electronicStore.web.dao;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
@Component("billingAddressDao")
public class BillingAddressDao {

	@Autowired
	private SessionFactory sessionFactory;

	public Session session() {
		return sessionFactory.getCurrentSession();
	}
	public void saveOrUpdate(BillingAddress billingAddress) {
		session().saveOrUpdate(billingAddress);
	}
	@SuppressWarnings("unchecked")
	public List<BillingAddress> getAll() {
		return session().createQuery("from BillingAddress").list();

	}
}
