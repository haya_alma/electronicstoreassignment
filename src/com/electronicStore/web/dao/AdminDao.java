package com.electronicStore.web.dao;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
@Component("adminDao")
public class AdminDao {

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private SessionFactory sessionFactory;

	public Session session() {
		return sessionFactory.getCurrentSession();
	}

	@Transactional()
	public void create(Admin admin) {
		admin.setPassword(passwordEncoder.encode(admin.getPassword()));
		session().save(admin);
	}
	
	public void saveOrUpdate(Admin admin) {
		session().saveOrUpdate(admin);
	}

	public boolean exists(String username) {
		Admin admin = getUser(username);
		return admin != null;
	}

	@SuppressWarnings("unchecked")
	public List<Admin> getAll() {
		return session().createQuery("from AdminAccount").list();

	}
	public Admin getUser(String username) {
		Criteria criteria = session().createCriteria(Admin.class);
		criteria.add(Restrictions.eq("username", username));
		return (Admin)criteria.uniqueResult();	
	}

}
