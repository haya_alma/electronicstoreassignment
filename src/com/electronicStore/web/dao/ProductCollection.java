package com.electronicStore.web.dao;
public interface ProductCollection {

	public void addProduct(Product product);
	
	public void removeProduct(Product product);
	
	public ProductIterator iterator(ProductCategoryEnum type);
	
	public ProductIterator manufacturerIterator(ProductManufacturerEnum type);
}
