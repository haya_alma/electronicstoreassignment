package com.electronicStore.web.dao;

import java.util.ArrayList;
import java.util.List;

public class ProductCollectionImpl implements ProductCollection {

	private List<Product> productsList;

	public ProductCollectionImpl() {
		productsList = new ArrayList<>();
	}

	public void addProduct(Product c) {
		this.productsList.add(c);
	}

	public void removeProduct(Product c) {
		this.productsList.remove(c);
	}

	@Override
	public ProductIterator iterator(ProductCategoryEnum type) {
		return new ProductIteratorImpl(type, this.productsList);
	}
	
	public ProductIterator manufacturerIterator(ProductManufacturerEnum productManufacturerEnum) {
		return new ProductIteratorImpl(productManufacturerEnum, this.productsList);
	}
}