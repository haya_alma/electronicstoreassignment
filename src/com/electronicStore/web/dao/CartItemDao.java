package com.electronicStore.web.dao;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
@Component("cartItemDao")
public class CartItemDao {

	@Autowired
	private SessionFactory sessionFactory;

	public Session session() {
		return sessionFactory.getCurrentSession();
	}

	@Transactional()
	public void create(CartItem cartItem) {
		session().save(cartItem);

	}
	
	public void update(CartItem cartItem) {
		session().update(cartItem);

	}
	
	@SuppressWarnings("unchecked")
	public List<CartItem> getAllCartItems() {
		return session().createQuery("from CartItem").list();

	}

	public CartItem getCartItem(String cartItemid) {
		Criteria criteria = session().createCriteria(CartItem.class);
		criteria.add(Restrictions.eq("CartItemid", cartItemid));
		return (CartItem)criteria.uniqueResult();
		
	}

	public void removeCartItem(CartItem cartItem) {
		session().delete(cartItem);		
	}

	public CartItem getCartItemByProductId(int productId) {		
		Criteria crit = session().createCriteria(CartItem.class)
		.createAlias("product", "p")
		.add(Restrictions.eq("p.productId", productId));
		return (CartItem) crit.uniqueResult();
	}

	/*
	 public CartItem getCartItemByCartId(int cartId) {
		Criteria criteria = session().createCriteria(CartItem.class)
		 .createAlias("cart", "c")
		 .add(Restrictions.eq("c.cartId", cartId));
		return (CartItem)criteria.uniqueResult();
	}
	*/
	
	public List<CartItem> getCartItemByCartId(int cartId) {
		return session().createCriteria(CartItem.class)
				 .createAlias("cart", "c")
				 .add(Restrictions.eq("c.cartId", cartId)).list();

	}
	

}
