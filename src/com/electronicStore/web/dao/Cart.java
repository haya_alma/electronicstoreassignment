package com.electronicStore.web.dao;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name="cart")
public class Cart implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="cartId")
    private int cartId;
    private BigDecimal totalPayment;
	
    public int getCartId() {
        return cartId;
    }

    public void setCartId(int cartId) {
        this.cartId = cartId;
    }

    public BigDecimal getTotalPayment() {
        return totalPayment;
    }

    public void setTotalPayment(BigDecimal totalPayment) {
        this.totalPayment = totalPayment;
    }

	@Override
	public String toString() {
		return "Cart [cartId=" + cartId + ", totalPayment=" + totalPayment + "]";
	}

	public void pay(PaymentStrategy paymentMethod){		
		BigDecimal amount = getTotalPayment();
		paymentMethod.pay(amount);
	}
    
}
