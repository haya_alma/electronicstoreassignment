package com.electronicStore.web.dao;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.stripe.model.Order;

@Repository
@Transactional
@Component("orderDao")
public class OrderDao {

	@Autowired
	private SessionFactory sessionFactory;

	public Session session() {
		return sessionFactory.getCurrentSession();
	}

	public void create(Orders orders) {
		session().save(orders);
	}
	public void update(Orders orders) {
		session().update(orders);
	}
	
	@SuppressWarnings("unchecked")
	public List<Orders> getAllOrders() {
		return session().createQuery("from Orders").list();
	}

	public void create(OrderItems orderItems) {
		session().save(orderItems);
	}
	public void update(OrderItems orderItems) {
		session().update(orderItems);
	}
	
	@SuppressWarnings("unchecked")
	public List<OrderItems> getAllOrderItems() {
		return session().createQuery("from OrderItems").list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Orders> getUserOrders(int userId) {
		Criteria crit = session().createCriteria(Orders.class);
		crit.createAlias("user", "u").add(Restrictions.eq("u.userid", userId));
		crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return crit.list();
	}

}
