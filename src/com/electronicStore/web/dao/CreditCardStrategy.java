package com.electronicStore.web.dao;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="CreditCardStrategy")
public class CreditCardStrategy implements PaymentStrategy, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
    private int id;
	private String cardholdername;
	private int cardNumber;
	private String dateOfExpiry;
	
	public CreditCardStrategy(String nm, int ccNum, String expiryDate){
		this.cardholdername=nm;
		this.cardNumber=ccNum;
		this.dateOfExpiry=expiryDate;
	}
	@Override
	public String toString() {
		return "CreditCardStrategy [id=" + id + ", cardholdername=" + cardholdername + ", cardNumber=" + cardNumber
				+ ", dateOfExpiry=" + dateOfExpiry + "]";
	}
	@Override
	public void pay(BigDecimal amount) {
		System.out.println(amount.toString() + " paid with credit/debit card");
		
	}

	
}