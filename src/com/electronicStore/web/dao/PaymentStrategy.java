package com.electronicStore.web.dao;

import java.math.BigDecimal;

public interface PaymentStrategy {
	public void pay(BigDecimal amount);

	
}

