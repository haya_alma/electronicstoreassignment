package com.electronicStore.web.dao;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Entity
@Table(name="OrderItems")
public class OrderItems implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
    private int id;
    private int itemQuantity;
    private String itemName;
	private BigDecimal cost;
	
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "orderId")
    private Orders order;
	
	public OrderItems(){
		
	}
	public OrderItems(int itemQuantity, String itemName, BigDecimal cost, Orders order) {
		this.itemQuantity = itemQuantity;
		this.itemName = itemName;
		this.cost = cost;
		this.order = order;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getItemQuantity() {
		return itemQuantity;
	}
	public void setItemQuantity(int itemQuantity) {
		this.itemQuantity = itemQuantity;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public BigDecimal getCost() {
		return cost;
	}
	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}
	public Orders getOrder() {
		return order;
	}
	public void setOrder(Orders order) {
		this.order = order;
	}
	@Override
	public String toString() {
		return "OrderItems [id=" + id + ", itemQuantity=" + itemQuantity + ", itemName=" + itemName + ", cost=" + cost
				+ ", order=" + order + "]";
	}
	
	
}
