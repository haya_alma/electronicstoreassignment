package com.electronicStore.web.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.electronicStore.web.dao.Admin;
import com.electronicStore.web.dao.Cart;
import com.electronicStore.web.dao.User;
import com.electronicStore.web.service.AdminService;
import com.electronicStore.web.service.CartService;
import com.electronicStore.web.service.UserService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

@Controller
public class UserController {
	@Autowired
	private UserService usersService;

	@Autowired
	private CartService cartService;

	@Autowired
	private SessionFactory sessionFactory;

	public Session session() {
		return sessionFactory.getCurrentSession();
	}
	
	@RequestMapping("/login")
	public void login(HttpServletResponse response) {
	}

	@RequestMapping("/logout")
	public String showLoggedOut() {
		return "/#/login";
	}
	
	@RequestMapping(value="/")
	public String showHome(){
		return "index";
	}
	
	@RequestMapping(value = "/account",  produces = "application/json")
	@ResponseBody
	public User account( Principal principal) {
		return 	usersService.getUser(principal.getName());
	}
	
	@RequestMapping(value="/allusers", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public Map<String, Object> getMessages(){
		
		List<User> usersList = null;
		
		usersList = usersService.getAllUsers();
		
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("users", usersList);
		
		return data;
	}
		
	@RequestMapping(value = "/createAccount")
	@ResponseBody
	 public User addUser(HttpServletResponse response , @RequestParam String username, @RequestParam String password,
				@RequestParam String email) throws IOException { 		
		User user = new User();
		user.setAuthority("ROLE_CUSTOMER");
		user.setEnabled(true);
		user.setUsername(username);
		user.setPassword(password);
		user.setEmail(email);
		user.setProfileImage("defaultProfile");

		Cart cart = new Cart();
		cart.setTotalPayment(new BigDecimal("0.00"));
		cartService.createCart(cart);
		user.setCart(cart);
		if (usersService.exists(username) ) {
			response.addHeader("message", "Caught duplicate username"); 
			return user;
		}else{	
			usersService.create(user);
		}
		return user;
	 }
}
