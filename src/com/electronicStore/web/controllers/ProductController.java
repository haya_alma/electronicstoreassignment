package com.electronicStore.web.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.electronicStore.web.dao.ProductCollectionImpl;
import com.electronicStore.web.dao.ProductIterator;
import com.electronicStore.web.dao.ProductManufacturerEnum;
import com.electronicStore.web.dao.ProductCategoryEnum;
import com.electronicStore.web.dao.Product;
import com.electronicStore.web.dao.ProductCollection;
import com.electronicStore.web.service.ProductService;


@Controller
public class ProductController {
	@Autowired
	private ProductService productService;

	@RequestMapping(value = "/newProduct", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	 public Product newProduct(HttpServletResponse response, @RequestParam String productName, 
			 @RequestParam String productCategory, @RequestParam String productType,
			 @RequestParam String productDescription, @RequestParam BigDecimal productPrice,
			 @RequestParam String productStatus, @RequestParam int unitInStock,
			 @RequestParam String productManufacturer,  @RequestParam String productImage) throws IOException {
		
				Product product = new Product();
				product.setProductCategory(productCategory);
				product.setProductDescription(productDescription);
				product.setProductImage(productImage);
				product.setProductManufacturer(productManufacturer);
				product.setProductName(productName);
				product.setProductPrice(productPrice);
				product.setProductStatus(productStatus);
				product.setUnitInStock(unitInStock);
				productService.saveOrUpdate(product);
				return product; 
	 }
	@RequestMapping(value = "/updateStock", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	 public Product updateStock(@RequestParam int productId, @RequestParam BigDecimal productPrice, @RequestParam int unitInStock) throws IOException {
		
				Product product = productService.getProductById(productId);		
				product.setProductPrice(productPrice);
				product.setUnitInStock(unitInStock);
				productService.saveOrUpdate(product);
				return product; 
	 }
	@RequestMapping(value = "/products", produces = "application/json")
	@ResponseBody
	public List<Product> getProducts() {
		return productService.getAllProducts();
	}
	
	@RequestMapping(value = "/getCategory", produces = "application/json")
	@ResponseBody
	public Map<String, Object> getProductsByCategory() {
		//@RequestParam String category
		String category="LAPTOP";
		Map<String, Object> data = new HashMap<String, Object>();
		List<Product> categoryList= new ArrayList<>();
		ProductCollection products = new ProductCollectionImpl();
		 for (Product product: productService.getAllProducts()){
			 products.addProduct(product);
		 }
		 ProductIterator categoryIterator = products.iterator(ProductCategoryEnum.valueOf(category));
		 	while (categoryIterator.hasNext()) {
				Product p = categoryIterator.next();
				categoryList.add(p);
			}
		 	
		data.put("category", categoryList);	
		return data;
	}
	
	@RequestMapping(value = "/getManufacturer", produces = "application/json")
	@ResponseBody
	public Map<String, Object> getProductsByManufacturer(@RequestParam String manufacturer) {
		Map<String, Object> data = new HashMap<String, Object>();
		List<Product> manufacturerList= new ArrayList<>();
		ProductCollection products = new ProductCollectionImpl();
		 for (Product product: productService.getAllProducts()){
			 products.addProduct(product);
		 }
		 ProductIterator manufacturerIterator = products.manufacturerIterator(ProductManufacturerEnum.valueOf(manufacturer));
		 	while (manufacturerIterator.hasNextM()) {
				Product p = manufacturerIterator.next();
				manufacturerList.add(p);
			}
		data.put("manufacturer", manufacturerList);	
		return data;
	}
	

}
