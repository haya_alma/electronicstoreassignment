package com.electronicStore.web.controllers;

import java.math.BigDecimal;
import java.security.Principal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Charge;
import com.stripe.model.Order;
import com.stripe.model.OrderItem;
import com.stripe.net.RequestOptions;
import com.electronicStore.web.dao.BillingAddress;
import com.electronicStore.web.dao.BillingAddressDao;
import com.electronicStore.web.dao.Cart;
import com.electronicStore.web.dao.CartItem;
import com.electronicStore.web.dao.CartItemI;
import com.electronicStore.web.dao.Product;
import com.electronicStore.web.dao.CartVisitor;
import com.electronicStore.web.dao.CartVisitorImpl;
import com.electronicStore.web.dao.CreditCardStrategy;
import com.electronicStore.web.dao.OrderItems;
import com.electronicStore.web.dao.Orders;
import com.electronicStore.web.dao.PaypalStrategy;
import com.electronicStore.web.dao.User;
import com.electronicStore.web.service.BillingAddressService;
import com.electronicStore.web.service.CartService;
import com.electronicStore.web.service.OrdersService;
import com.electronicStore.web.service.PaymentService;
import com.electronicStore.web.service.ProductService;
import com.electronicStore.web.service.UserService;

@Controller
public class OrderController {
	@Autowired
	private UserService userService;

	@Autowired
	private OrdersService ordersService;
	@Autowired
	private BillingAddressService billingAddressService;
	
	private User user;
	
	public User getUser(Principal principal) {
		return userService.getUser(principal.getName());
	}
	
	@RequestMapping(value="/allOrders", produces = "application/json")
	@ResponseBody
	public Map<String, Object> allOrders(){
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("orders", ordersService.getAllOrders());
		data.put("items", ordersService.getAllOrderItems());
		return data;
	}
	@RequestMapping(value="/userOrders", produces = "application/json")
	@ResponseBody
	public List<Orders> userOrders(Principal principal){
		return ordersService.getUserOrders(getUser(principal).getUserid());
	}
	
	@RequestMapping(value="/addressList", produces = "application/json")
	@ResponseBody
	public List<BillingAddress> addressList(){
		return billingAddressService.getAll();
	}
	
	
}
