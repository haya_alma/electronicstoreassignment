package com.electronicStore.web.controllers;

import java.math.BigDecimal;
import java.security.Principal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Charge;
import com.stripe.model.Order;
import com.stripe.model.OrderItem;
import com.stripe.net.RequestOptions;
import com.electronicStore.web.dao.BillingAddress;
import com.electronicStore.web.dao.BillingAddressDao;
import com.electronicStore.web.dao.Cart;
import com.electronicStore.web.dao.CartItem;
import com.electronicStore.web.dao.CartItemI;
import com.electronicStore.web.dao.Product;
import com.electronicStore.web.dao.CartVisitor;
import com.electronicStore.web.dao.CartVisitorImpl;
import com.electronicStore.web.dao.CreditCardStrategy;
import com.electronicStore.web.dao.OrderItems;
import com.electronicStore.web.dao.Orders;
import com.electronicStore.web.dao.PaypalStrategy;
import com.electronicStore.web.dao.User;
import com.electronicStore.web.service.BillingAddressService;
import com.electronicStore.web.service.CartService;
import com.electronicStore.web.service.OrdersService;
import com.electronicStore.web.service.PaymentService;
import com.electronicStore.web.service.ProductService;
import com.electronicStore.web.service.UserService;

@Controller
public class CartController {
	@Autowired
	private UserService userService;
	@Autowired
	private CartService cartService;
	@Autowired
	private ProductService productService;
	@Autowired
	private PaymentService paymentService;
	@Autowired
	private OrdersService ordersService;
	@Autowired
	private BillingAddressService billingAddressService;
	private User user;
	public Cart getUserCart(Principal principal) {
		user = userService.getUser(principal.getName());
		return user.getCart();
	}
	
	@RequestMapping(value="/carts" , method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Cart> getCarts() {
		return cartService.getAllCarts();
	}
	
	@RequestMapping(value="/getCart", method = RequestMethod.GET, produces ="application/json")
	@ResponseBody
	public Map<String, Object> getCartById(Principal principal) {
		Map<String, Object> data = new HashMap<String, Object>();
		List<CartItem> cartitems =  (List<CartItem>) cartService.getCartItemByCartId(getUserCart(principal).getCartId());
		data.put("cart", getUserCart(principal));
		data.put("cartitems",cartitems);
		return data;
	}
	private BigDecimal calculatePrice(List<CartItem> cartItems) {
		CartVisitor visitor = new CartVisitorImpl();
		BigDecimal sum = new BigDecimal("0.00");
		for(CartItem item : cartItems){
			sum = sum.add(((CartItemI) item).accept(visitor));
			cartService.updateCartItem(item);
		}
		return sum;
	}
	@RequestMapping(value = "/cart/add", produces ="application/json")
	@ResponseBody
	public Map<String, Object> addItem( @AuthenticationPrincipal User activeUser, Principal principal,
			@RequestParam int productId, @RequestParam int quantity) {
		Map<String, Object> data = new HashMap<String, Object>();
			Cart cart = getUserCart(principal);
			Product product = productService.getProductById(productId);
			int stock =product.getUnitInStock() - quantity;
			if(stock == 0){ product.setProductStatus("out of stock");}
			product.setUnitInStock(stock);
			productService.saveOrUpdate(product);
			
			CartItem cartItem = new CartItem();
			cartItem.setProduct(product);
			cartItem.setQuantity(quantity);
			cartItem.setTotalPrice(product.getProductPrice().multiply(new BigDecimal(quantity)));
			cartItem.setCart(getUserCart(principal));
			cartItem.setCost(product.getProductPrice().multiply(new BigDecimal(quantity)));
			cartItem.setDiscount(new BigDecimal("0.00"));
			cartService.addCartItem(cartItem);
			
			List<CartItem> cartItems = cartService.getCartItemByCartId(cart.getCartId());
			cart.setTotalPayment(calculatePrice(cartItems));
			cartService.updateCart(cart);
	
			List<CartItem> cartItemsNew = cartService.getCartItemByCartId(cart.getCartId());
			data.put("cartItems", cartItemsNew);
		return data;
	}

	@RequestMapping(value = "/cart/remove", produces = "application/json")
	@ResponseBody
	public void removeItem(@RequestParam int productId) {
		CartItem cartItem = cartService.getCartItemByProductId(productId);		
		cartService.removeCartItem(cartItem);
	}
	@RequestMapping(value = "/cart/removeAll", produces = "application/json")
	@ResponseBody
	public void clearCart(@RequestParam int cartId) {
		Cart cart = cartService.getCartById(cartId);
		cart.setTotalPayment(new BigDecimal("0.00"));
		cartService.updateCart(cart);
		List<CartItem> cartItems= cartService.getCartItemByCartId(cartId);
		for(int i=0; i<cartItems.size(); i++){	
			cartService.removeCartItem(cartItems.get(i));
		}
	}
	@RequestMapping(value="/payment", produces = "application/json")
	@ResponseBody
	public void payment(Principal pr, @RequestParam String token, @RequestParam int totalPayment, @RequestParam String selectedPayment,
					@RequestParam String email, @RequestParam int cardNumber, @RequestParam String carholdername, @RequestParam String expireDate,
					@RequestParam String addr1, @RequestParam String addr2, @RequestParam String city,@RequestParam String country ) {		

			BigDecimal total=BigDecimal.valueOf(totalPayment);
			Cart cart = getUserCart(pr);
			cart.setTotalPayment(total);
		if(selectedPayment.equals("creditCard")){
				CreditCardStrategy cardStrategy = new CreditCardStrategy(carholdername, cardNumber, expireDate);
				paymentService.saveCard(cardStrategy);
				cart.pay(cardStrategy);
				Stripe.apiKey = "sk_test_aQD7Da1yURGaS8fnuPB8EBLs";
				Map<String, Object> chargeParams = new HashMap<String, Object>();
				chargeParams.put("amount", totalPayment * 100);
				chargeParams.put("currency", "eur");
				chargeParams.put("source", token);
				try {
					Charge charge = Charge.create(chargeParams);
				} catch (AuthenticationException | InvalidRequestException | APIConnectionException | CardException | APIException e) {
					e.printStackTrace();
				}
		}
		addNewOrder(pr, selectedPayment, total,  addr1,  addr2,  city,  country);
		clearCart(cart.getCartId());
	}	
	
	@RequestMapping(value="/paymentP", produces = "application/json")
	@ResponseBody
	public void paymentPaypal(Principal pr, @RequestParam int totalPayment, @RequestParam String selectedPayment,
					@RequestParam String email,	@RequestParam String addr1, @RequestParam String addr2,
					@RequestParam String city,@RequestParam String country ) {		

			BigDecimal total=BigDecimal.valueOf(totalPayment);
			Cart cart = getUserCart(pr);
			cart.setTotalPayment(total);
		 if(selectedPayment.equals("Paypal")){
			PaypalStrategy paypalStrategy = new PaypalStrategy(email);
			paymentService.savePaypal(paypalStrategy);
			cart.pay(paypalStrategy);
		}
		addNewOrder(pr, selectedPayment, total,  addr1,  addr2,  city,  country);
		clearCart(cart.getCartId());
	}
	public void addNewOrder(Principal principal, String selectedPayment, BigDecimal total, 
			String addr1, String addr2, String city, String country ) {
		Orders order = new Orders(selectedPayment, total, new Timestamp(System.currentTimeMillis()), user);
		ordersService.addOrder(order);
		order.getId();
		List<CartItem> cartItems= cartService.getCartItemByCartId(getUserCart(principal).getCartId());
		for(int i=0; i<cartItems.size(); i++){	
			OrderItems orderItem = new OrderItems(cartItems.get(i).getQuantity(), cartItems.get(i).getProduct().getProductName(), 
												cartItems.get(i).getCost(), order);
			ordersService.addOrderItems(orderItem);
		}
		BillingAddress billingAddress = new BillingAddress();
		billingAddress.setAddr1(addr1);
		billingAddress.setAddr2(addr2);
		billingAddress.setCity(city);
		billingAddress.setCountry(country);
		billingAddress.setOrder(order);
		billingAddressService.saveOrUpdate(billingAddress);
		
	}
}
