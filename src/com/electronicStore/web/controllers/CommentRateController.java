package com.electronicStore.web.controllers;

import java.security.Principal;
import java.sql.Timestamp;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.electronicStore.web.dao.Comment;
import com.electronicStore.web.dao.Product;
import com.electronicStore.web.dao.Rate;
import com.electronicStore.web.dao.User;
import com.electronicStore.web.service.CommentRateService;
import com.electronicStore.web.service.ProductService;
import com.electronicStore.web.service.UserService;

@Controller
public class CommentRateController {
	
	@Autowired
	CommentRateService commentRateService;
	@Autowired
	UserService userService;
	@Autowired
	ProductService productService;
	
	@RequestMapping(value="/commentsList", produces = "application/json")
	@ResponseBody
	 public List<Comment> commentsList(){
		return commentRateService.allComments();
	}
	@RequestMapping(value="/addComment", produces = "application/json")
	@ResponseBody
	 public void addComment(Principal principal, @RequestParam String commentText, @RequestParam int productId){
		System.out.println(commentText + productId);
		Comment comment = new Comment();
		comment.setComment(commentText);
		Product product = productService.getProductById(productId);
		comment.setProduct(product);
		User user = userService.getUser(principal.getName());
		comment.setUser(user);
		comment.setPosted(new Timestamp(System.currentTimeMillis()));
		commentRateService.addComment(comment);
	}
	@RequestMapping("/removeComment")
	@ResponseBody
	public void deleteComment(@RequestParam int commentId) {
		Comment comment = new Comment();
		comment.setId(commentId);
		commentRateService.removeComment(comment);
	}

	@RequestMapping(value="/ratesList", produces = "application/json")
	@ResponseBody
	 public List<Rate> ratesList(){
		return commentRateService.allRates();
	}
	@RequestMapping(value="/addRate", produces = "application/json")
	@ResponseBody
	 public void addRate(Principal principal, @RequestParam int rating, @RequestParam int productId){
		Rate rate = new Rate();
		rate.setRating(rating);
		Product product = productService.getProductById(productId);
		rate.setProduct(product);
		commentRateService.addRate(rate);
	}
}
