package com.electronicStore.web.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.electronicStore.web.dao.Admin;
import com.electronicStore.web.dao.Cart;
import com.electronicStore.web.dao.User;
import com.electronicStore.web.service.AdminService;
import com.electronicStore.web.service.CartService;
import com.electronicStore.web.service.UserService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

@Controller
public class AdminController {

	@Autowired
	private AdminService adminService;
	@Autowired
	private CartService cartService;

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	PasswordEncoder passwordEncoder;


	public Session session() {
		return sessionFactory.getCurrentSession();
	}
	
	@RequestMapping("/adminlogin")
	@ResponseBody
	public Admin login(@RequestParam String username, @RequestParam String password) {
		Admin admin = adminService.getUser(username);
		if(passwordEncoder.matches(password, admin.getPassword())){
			return admin;
		}else {
			return null;
		}
	}
	
	public Admin addAdminAccount(){ 		
		Admin admin = new Admin();
		admin.setUsername("Admin678902");
		admin.setPassword("Admin678902");
		adminService.create(admin);
		return admin;
	 }
}
