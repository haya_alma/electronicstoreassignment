package com.electronicStore.web.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.electronicStore.web.dao.Comment;
import com.electronicStore.web.dao.CommentRateDao;
import com.electronicStore.web.dao.Rate;

@Service("commentService")
public class CommentRateService {
	
	@Autowired
	private CommentRateDao commentDao;
	
	public void addComment(Comment comment) {
		commentDao.saveOrUpdate(comment);
	}
	public void removeComment(Comment comment) {
		commentDao.delete(comment);
	}
	public List<Comment> allComments(){
		return commentDao.allComments();
	}
	public void addRate(Rate rate) {
		commentDao.saveOrUpdate(rate);
	}
	public List<Rate> allRates(){
		return commentDao.allRates();
	}
}
