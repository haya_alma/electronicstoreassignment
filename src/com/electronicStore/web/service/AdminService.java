package com.electronicStore.web.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.electronicStore.web.dao.Admin;
import com.electronicStore.web.dao.AdminDao;
import com.electronicStore.web.dao.UserDao;

@Service("adminService")
public class AdminService {
	@Autowired
	private AdminDao adminDao;

	public void create(Admin admin) {
		adminDao.create(admin);
	}
	public void saveOrUpdate(Admin admin) {
		adminDao.saveOrUpdate(admin);
	}
	public boolean exists(String username) {
		return adminDao.exists(username);
	}
	public Admin getUser(String username) {
		return adminDao.getUser(username);
	}

	public List<Admin> getAll() {
		return adminDao.getAll();
	}
	
}
