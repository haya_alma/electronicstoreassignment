package com.electronicStore.web.service;

import java.sql.SQLException;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.electronicStore.web.dao.Product;
import com.electronicStore.web.dao.ProductDao;

@Service("productService")
public class ProductService {

		@Autowired
		private ProductDao productDao;

		public void saveOrUpdate(Product product) {
			productDao.saveOrUpdate(product);
		}

		public Product getProductById(int productId) {
			return productDao.getProduct(productId);
		}
		
		public List<Product> getAllProducts() {
			return productDao.getAllProducts();
		}
		
}

