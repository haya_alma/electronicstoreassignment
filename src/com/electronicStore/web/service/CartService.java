package com.electronicStore.web.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.electronicStore.web.dao.Cart;
import com.electronicStore.web.dao.CartDao;
import com.electronicStore.web.dao.CartItem;
import com.electronicStore.web.dao.CartItemDao;
import com.electronicStore.web.dao.CartItemI;

@Service("cartService")
public class CartService {

		@Autowired
		private CartDao cartDao;
		
		@Autowired
		private CartItemDao cartItemDao;
		
		
/////////////		Cart  Service	//////////////////
		public void createCart(Cart cart) {
			cartDao.create(cart);
		}
		public void updateCart(Cart cart) {
			cartDao.update(cart);
		}

		public boolean cartExists(int id) {
			return cartDao.exists(id);
		}
		public Cart getCartById(int cartId) {
			return cartDao.getCartById(cartId);
		}
		
		public List<Cart> getAllCarts() {
			return cartDao.getAllCarts();
		}
		
/////////////		CartItem Service	//////////////////
		public void addCartItem(CartItem cartItem) {
			cartItemDao.create(cartItem);
		}
		public void updateCartItem(CartItem cartItem) {
			cartItemDao.update(cartItem);
		}
		public List<CartItem> getAllCartItems() {
			return cartItemDao.getAllCartItems();
		}
		
		public void removeCartItem(CartItem cartItem) {
		        cartItemDao.removeCartItem(cartItem);
		    }

		    public CartItem getCartItemByProductId (int productId) {
		        return cartItemDao.getCartItemByProductId(productId);
		    }
		    public List<CartItem> getCartItemByCartId (int cartId) {
		        return cartItemDao.getCartItemByCartId(cartId);
		    }
	
}
