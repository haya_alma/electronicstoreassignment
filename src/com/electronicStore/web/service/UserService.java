package com.electronicStore.web.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.electronicStore.web.dao.User;
import com.electronicStore.web.dao.UserDao;

@Service("userService")
public class UserService {
	@Autowired
	private UserDao usersDAO;

	public void create(User user) {
		usersDAO.create(user);
	}
	public void saveOrUpdate(User user) {
		usersDAO.saveOrUpdate(user);
	}
	public boolean exists(String username) {
		return usersDAO.exists(username);
	}
	public User getUser(String username) {
		return usersDAO.getUser(username);
	}

	public List<User> getAllUsers() {
		return usersDAO.getAllUsers();
	}
	
}
