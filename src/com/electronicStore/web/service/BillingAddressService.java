package com.electronicStore.web.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.electronicStore.web.dao.BillingAddress;
import com.electronicStore.web.dao.AdminDao;
import com.electronicStore.web.dao.BillingAddressDao;
import com.electronicStore.web.dao.UserDao;

@Service("BillingAddressService")
public class BillingAddressService {
	@Autowired
	private BillingAddressDao billingAddressDao;

	public void saveOrUpdate(BillingAddress billingAddress) {
		billingAddressDao.saveOrUpdate(billingAddress);
	}
	public List<BillingAddress> getAll() {
		return billingAddressDao.getAll();
	}
	
}
