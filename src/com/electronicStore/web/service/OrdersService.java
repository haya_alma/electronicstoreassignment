package com.electronicStore.web.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.electronicStore.web.dao.Orders;
import com.electronicStore.web.dao.CartDao;
import com.electronicStore.web.dao.CartItem;
import com.electronicStore.web.dao.CartItemDao;
import com.electronicStore.web.dao.CartItemI;
import com.electronicStore.web.dao.OrderDao;
import com.electronicStore.web.dao.OrderItems;

@Service("orderService")
public class OrdersService {
		@Autowired
		private OrderDao orderDao;
		
		public void addOrder(Orders orders) {
			orderDao.create(orders);
		}
		public void updateCart(Orders orders) {
			orderDao.update(orders);
		}
		public List<Orders> getAllOrders() {
			return orderDao.getAllOrders();
		}
		public void addOrderItems(OrderItems orderItems) {
			orderDao.create(orderItems);
		}
		public void updateCart(OrderItems orderItems) {
			orderDao.update(orderItems);
		}
		public List<OrderItems> getAllOrderItems() {
			return orderDao.getAllOrderItems();
		}
		public List<Orders> getUserOrders(int userId) {
			return orderDao.getUserOrders(userId);
		}
}
