package com.electronicStore.web.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.electronicStore.web.dao.Cart;
import com.electronicStore.web.dao.CartDao;
import com.electronicStore.web.dao.CartItem;
import com.electronicStore.web.dao.CartItemDao;
import com.electronicStore.web.dao.CartItemI;
import com.electronicStore.web.dao.CreditCardStrategy;
import com.electronicStore.web.dao.PaymentDao;
import com.electronicStore.web.dao.PaypalStrategy;

@Service("paymentService")
public class PaymentService {

		@Autowired
		private PaymentDao paymentDao;

		public void saveCard(CreditCardStrategy cardStrategy) {
			paymentDao.saveCard(cardStrategy);
		}
		public List<CreditCardStrategy> getAllCreditCard() {
			return paymentDao.getAllCreditCardStrategy();
		}
		public void savePaypal(PaypalStrategy paypalStrategy) {
			paymentDao.savePaypal(paypalStrategy);
		}
		public List<PaypalStrategy> getAllPaypalStrategy() {
			return paymentDao.getAllPaypalStrategy();
		}
}
