module.exports = function (grunt) {

    // Project configuration.
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        concat: {
            js: {
                src: ['app/scripts/**/**/*.js'],
                dest: 'build/js/scripts.js'
            },
            css: {
                src: ['app/css/**/**/*.scss'],
                dest: 'build/css/styles.scss'
            },
            html: {
                src: ['app/views/**/**/*.html'],
                dest: 'build/app/main.html'
            },
        },
        watch: {
            js: {
                files: ['app/scripts/**/**/*.js'],
                tasks: ['concat:js'],
                options: {
                    livereload: true,
                }
            },
            css: {
                files: ['app/css/**/**/*.scss'],
                tasks: ['concat:css'],
                options: {
                    livereload: true,
                }
            },
            html: {
                files: ['app/**/**/**/*.html'],
                tasks: ['concat:html'],
                options: {
                    livereload: true,
                }
            },
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('default', ['concat','watch']);
};