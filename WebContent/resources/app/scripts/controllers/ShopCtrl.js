angular.module('ElectronicStore.ShopCtrl', []).controller('ShopCtrl',
		function($scope, $http, $cookieStore, $state, $window, $interval) {
	
	$scope.addComment = function(productId, comment){
		if(!angular.isUndefined($scope.thisUser)){
   	 	$http({
		        method: 'POST',
		        url: '/ElectronicStore/addComment',
		        data: $.param({
		        	productId: parseInt(productId),
		        	commentText: comment
		        	}),
		        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    }).then(function(response) {		  
		    	 document.getElementById("commentForm").reset();
		    	 if(response.status == 200 ){
						$window.location.reload();
					}
		    });
		 }else if(angular.isUndefined($scope.thisUser)){
	  			$state.go('login');
		  }
	   }
	$scope.deleteComment = function(commentId){
		if(!angular.isUndefined($scope.thisUser)){
   	 	$http({
		        method: 'POST',
		        url: '/ElectronicStore/removeComment',
		        data: $.param({
		        	commentId: commentId
		        	}),
		        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    });
		  }else if(angular.isUndefined($scope.thisUser)){
	  			$state.go('login');
		  }
		}
		
		$scope.viewItem = function (product) {
			$state.go("viewItem");
			$scope.product =product
	        $window.sessionStorage.setItem('product', JSON.stringify(product));
		}	
		if($scope.product == null){
			var temp = sessionStorage.getItem('product');
			$scope.product = $.parseJSON(temp);	
		}
		
		$http.get('/ElectronicStore/commentsList').
	    success(function(response) {
	    	$scope.commentsList = response;
	    });
		
		$http.get('/ElectronicStore/ratesList').
	    then(function(response) {
	    	$scope.ratesList = response.data;
	    	$scope.reviewAverageList=[];	
			
	    	$scope.dataReviews = $scope.ratesList, grouped = Object.create(null);
	    	$scope.dataReviews.forEach(function (a) {
			    grouped[a.product.productId] = grouped[a.product.productId] || [];
			    grouped[a.product.productId].push(a);
			});
			 for(var i in grouped) { 
				 	var ratingTotal=0; 
					for(var x = 0; x< grouped[i].length;  x++) {
					     ratingTotal += grouped[i][x].rating; 
					 }
	    			 $scope.Total = grouped[i].length * 10;
	    			 $scope.ratingAverage = (ratingTotal / $scope.Total) * 10;
	    			 $scope.reviewAverageList.push({id : i, rating : $scope.ratingAverage.toFixed(2) });
			 }	
	    });
	
		
		  $scope.getCategory = function (category) {
			  console.log(category)
			  $http({
		        method: 'POST',
		        url: '/ElectronicStore/getCategory',
		        data: $.param({
		        	category: category
		        	}),
		        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    }).then(function(response) {
		    	$scope.productsList = response.data.category;
		    });
		};
		  $scope.getBrand = function (manufacturer) {
			  $http({
		        method: 'POST',
		        url: '/ElectronicStore/getManufacturer',
		        data: $.param({
		        	manufacturer: manufacturer
		        	}),
		        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    }).then(function(response) {
		    	$scope.productsList = response.data.manufacturer;
		    });
		};
		
	    $scope.rating1 = 0;

	    $scope.click1 = function (param, productId) {
	        console.log('Click || ' + param );
	    	$http({
		        method: 'POST',
		        url: '/ElectronicStore/addRate',
		        data: $.param({
		        	productId: parseInt(productId),
		        	rating: param
		        	}),
		        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    });
	    }
			$scope.addItem = function (productId, quantity) {	
				console.log(productId, quantity);
				 if(!angular.isUndefined($scope.thisUser)){
						$scope.itemAdded = false;
						for (var j=0; j < $scope.cartItems.length; j++) {
							if(productId == $scope.cartItems[j].product.productId ){
								$scope.itemAdded = true;
							}
						}
						if( false == $scope.itemAdded ){ 
								$http({
						        method: 'POST',
						        url: '/ElectronicStore/cart/add',
						        data: $.param({
						        	productId: productId,
						        	quantity: parseInt($scope.quantity)
						        	}),
						        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
						    }).then(function(response) {
								if(response.status == 200 ){
									var timer=$interval(function(){
										var x=0;
										x++;
										$window.onload = function what(){
										var addButton=	document.getElementById("addItemBtn");
										 addButton.innerHTML="Added";
											};
								    },300);
								}else {
									console.log('error');
								}
							});
						}
						
				 }else if(angular.isUndefined($scope.thisUser)){
			  			$state.go('login');
				  }
			}
			
  }).run(function($rootScope, $state, $http, $timeout, $interval,$cookieStore) {
	  $http.get('/ElectronicStore/products').
	    success(function(response) {
	    	$rootScope.productsList = response;
	    	console.log($rootScope.productsList)
	    });
	  
	  $rootScope.propertyName = 'productName';
	  $rootScope.reverse = true;

	  $rootScope.sortBy = function(propertyName) {
		  $rootScope.reverse = ($rootScope.propertyName === propertyName) ? !$rootScope.reverse : false;
		  $rootScope.propertyName = propertyName;
	  };
  });