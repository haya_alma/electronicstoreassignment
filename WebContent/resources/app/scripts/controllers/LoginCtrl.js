angular.module('ElectronicStore.LoginCtrl', []).controller('LoginCtrl',
		function($scope, $rootScope, $http, $cookieStore, $state, $window) {

	$scope.LoginButton = function (){ 
			if($scope.username != null && $scope.password != null){
					    event.preventDefault();
					    $('form').fadeOut(500);
					    $('.wrapper').addClass('form-success');
			 }
		}
  })
.run(function($rootScope, $cookieStore, $state) {	
	$rootScope.checkBoxClicked = function( checkboxModel){
		$cookieStore.put('rememberMe', checkboxModel);
	}
	$rootScope.checkboxModel= $cookieStore.get('rememberMe');
	
	$rootScope.test="Testing this";
	if(!$cookieStore.get('user')){
		$state.go('login');
	}
		  	
});
