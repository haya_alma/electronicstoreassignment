angular.module('ElectronicStore.CustomerCtrl', []).controller('CustomerCtrl', 
		function($scope, $rootScope, $http, $cookieStore, $cookies,  $state, $window, $sce, $interval) {
		$scope.checkboxModel=true;
		 $scope.items = [];
	        $scope.add = function () {
	          $scope.items.push({ 
	             product: ""
	          });
	        };
			$http.get('/ElectronicStore/account').then(function(response) {
				if (true == $scope.checkboxModel){
					var now = new Date(), expiresValue = new Date(now.getFullYear()+1, now.getMonth(), now.getDate());
					$cookieStore.put('user', response.data,  {'expires' : expiresValue});
				}else if (false == $scope.checkboxModel){
					 var today = new Date();
					 var expiresValue = new Date(today);
					 expiresValue.setMinutes(today.getMinutes() + 60);
					$cookieStore.put('user', response.data, {'expires' : expiresValue});
				}
			});
		if('true' == angular.isUndefined($cookies.getObject('user'))){
			$state.go('login');
		} 
		 $scope.UpdateAccount = function () {
			$http({
		        method: 'POST',
		        url: '/ElectronicStore/updateAccountDetails',
		        data: $.param({
		        	id: $scope.id,
		            firstname: $scope.firstname,
		            lastname: $scope.lastname,
		            email: $scope.email,
		            descr: $scope.descr 
		        }),
		        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    });
			 modal.style.display = "none";
		 }
		 
		 $http.get('/ElectronicStore/userOrders').then(function(response) {
			 console.log(response.data);
			 $scope.userOrders = [];
			 for (var j=0; j < response.data.length; j++) {
					var dateStr = JSON.parse(response.data[j].date);  					        
					response.data[j].date = new Date(dateStr);
					$scope.userOrders.push(response.data[j]);
				}
		 });
		 $http.get('/ElectronicStore/allOrders').then(function(response) {
			 $scope.allOrders = [];
			 for (var o=0; o< response.data.items.length; o++) {
					var dateStr = JSON.parse(response.data.items[o].order.date);  					        
					response.data.items[o].order.date = new Date(dateStr);
					$scope.allOrders.push(response.data.items[o]);
				}
		 });
		 $http.get('/ElectronicStore/addressList').then(function(response) {
			 $scope.addressList = response.data;
		 });
});
