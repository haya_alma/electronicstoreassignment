angular.module('ElectronicStore.AdminCtrl', []).controller('AdminCtrl', 
		function($scope, $rootScope, $http, $cookieStore, $cookies,  $state, $window, $sce, $interval) {
	$scope.adminLogin= function(){
		console.log($scope.adminUsername + " | " + $scope.adminPassword);
		$http({
	        method: 'POST',
	        url: '/ElectronicStore/adminlogin',
	        data: $.param({
	            username: $scope.adminUsername,
	            password: $scope.adminPassword
	            }),
	        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    }).then(function(response) {
			console.log(response);
			if(response.status == 200){
				$cookieStore.put('admin', response.data);
			    $state.go('admin');
			}	
		});
	}
	if(!$cookieStore.get('admin')){
		$state.go('adminlogin');
	}else{
		
		
		
	$scope.updateStock = function(product){
		console.log(product);
		$scope.item = product;
		var modal = document.getElementById("itemModal");
		modal.style.display = "block";
		
		 window.onclick = function (event) {
		        if (event.target == modal) {
		            modal.style.display = "none";
		        }
		     }
	}
	$scope.updateItem = function (productId, unitInStock, productPrice) {
		$http({
		        method: 'POST',
		        url: '/ElectronicStore/updateStock',
		        data: $.param({
		        	productId:productId,
		        	unitInStock: unitInStock, 
		        	productPrice: productPrice
		          }),
		        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    }).then(function (response) {
		    	if(response.status == 200){
		    		$state.go('stock');
		    		var modal = document.getElementById("itemModal");
		    		modal.style.display = "none";
		    	}
		    });
		}
	      
	$scope.thisAdmin=$cookieStore.get('admin');
	
	$http.get('/ElectronicStore/allOrders').then(function(response) {
		 console.log(response.data)
		 $scope.orders = response.data.orders;
		 $scope.items = response.data.items;
	 });
	 $http.get('/ElectronicStore/addressList').then(function(response) {
		 $scope.addressList = response.data;
	 });
	
	}
});
