angular.module('ElectronicStore.PaymentCtrl', [])
.controller('PaymentCtrl', function($scope,$http,$cookies, $window, $state) {

	$scope.billing = function(){
		$scope.paypal ='';		
	}
	
	$scope.delivery = function(){
		$scope.creditCard = '';
	}
	
	$scope.shopGo = function(){
		$state.go('products');
	}

	$scope.payPalSubmit= function(){
	    
    	$http({
	        method: 'POST',
	        url: '/ElectronicStore/paymentP',
	        data: $.param({
	        	totalPayment: parseInt($scope.totalPayment),
	        	selectedPayment: "Paypal",
				email: $scope.payPalEmail, 
				addr1:$scope.addr1,
				addr2:$scope.addr2,
				city:$scope.city,
				country:$scope.country
	        	}),
	        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    }).then(function(response) {
			if(response.status == 200 ){
				console.log(200);
			    $state.go('/');
			}	
		});
	}
	
	var stripe = Stripe('pk_test_OPz3O0SbCrzKE5QrON0QfA0r');
	var elements = stripe.elements();

	var card = elements.create('card', {
	  iconStyle: 'solid',
	  style: {
	    base: {
	      iconColor: '#8898AA',
	      color: 'white',
	      lineHeight: '36px',
	      fontWeight: 300,
	      fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
	      fontSize: '16px',

	      '::placeholder': {
	        color: '#8898AA',
	      },
	    },
	    invalid: {
	      iconColor: '#e85746',
	      color: '#e85746',
	    }
	  },
	  classes: {
	    focus: 'is-focused',
	    empty: 'is-empty',
	  },
	});
	card.mount('#card-element');

	var inputs = document.querySelectorAll('input.field');
	Array.prototype.forEach.call(inputs, function(input) {
	  input.addEventListener('focus', function() {
	    input.classList.add('is-focused');
	  });
	  input.addEventListener('blur', function() {
	    input.classList.remove('is-focused');
	  });
	  input.addEventListener('keyup', function() {
	    if (input.value.length === 0) {
	      input.classList.add('is-empty');
	    } else {
	      input.classList.remove('is-empty');
	    }
	  });
	});

	function setOutcome(result) {
	  var successElement = document.querySelector('.success');
	  var errorElement = document.querySelector('.error');
	  successElement.classList.remove('visible');
	  errorElement.classList.remove('visible');
	
	  if (result.token) {
	    successElement.querySelector('.token').textContent = result.token.id;
	    successElement.classList.add('visible');
	    
    	$http({
	        method: 'POST',
	        url: '/ElectronicStore/payment',
	        data: $.param({
	        	token: result.token.id,
	        	totalPayment: parseInt($scope.totalPayment),
	        	selectedPayment: "creditCard",
				email: $scope.email, 
				cardNumber: parseInt(result.token.card.last4), 
				carholdername: $scope.cardholdername,
				expireDate: $scope.expireDate,
				addr1:$scope.addr1,
				addr2:$scope.addr2,
				city:$scope.city,
				country:$scope.country
	        	}),
	        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    }).then(function(response) {
			if(response.status == 200 ){
				console.log(200);
			    $state.go('/');
			}	
		});
	    
	  } else if (result.error) {
	    errorElement.textContent = result.error.message;
	    errorElement.classList.add('visible');
	  }
	}

	card.on('change', function(event) {
	  setOutcome(event);
	});

	document.querySelector('form').addEventListener('submit', function(e) {
	  e.preventDefault();
	  var form = document.querySelector('form');
	  var extraDetails = {
	    name: form.querySelector('input[name=cardholder-name]').value,
	  };
	  stripe.createToken(card, extraDetails).then(setOutcome);
	});
});