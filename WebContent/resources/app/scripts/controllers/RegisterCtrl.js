angular.module('ElectronicStore.RegisterCtrl', [])
.controller('RegisterCtrl', function($scope,$http,$cookies, $window, $state) {

	
	$scope.Register = function () {
		if($scope.regUsername != null || $scope.regPassword != null || $scope.regEmail != null){
			$http({
		        method: 'POST',
		        url: '/ElectronicStore/createAccount',
		        data: $.param({
		            username: $scope.regUsername,
		            password: $scope.regPassword,
		            email: $scope.regEmail
		          }),
		        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    }).success(function (data, status, headers, config) {
		    	console.log('s >> ' + headers()['message']);
		    	if(headers()['message'] != null){
			    	$scope.message = headers()['message'];
		    	}else {
		    		$state.go('login');
		    	}
		    }).error(function (data, status, headers, config) {
		    	console.log('error response >> ' + headers()['message']);
		    });
		}else if($scope.regUsername == null || $scope.regPassword == null || $scope.regEmail == null){
			$scope.message = "Enter Your Details !";
		}
	}
	
	$(function() {
	    $("#confirmPassword").keyup(function() {
	        var password = $("#regPassword").val();
	        $("#divCheckPasswordMatch").html(password == $(this).val() ? "Passwords match." : "Passwords do not match!");
	    });

	});

  });
