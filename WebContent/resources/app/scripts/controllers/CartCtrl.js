angular.module('ElectronicStore.CartCtrl', []).controller('CartCtrl', 
		function($scope, $scope, $http, $cookieStore, $cookies,  $state, $window, $sce, $interval) {  
	
	$scope.deleteItem = function(productId){
    	$http({
	        method: 'POST',
	        url: '/ElectronicStore/cart/remove',
	        data: $.param({
	        	productId: productId
	        	}),
	        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    }).then(function(response) {
			if(response.status == 200 ){
				$window.location.reload();
			}else {
				console.log('error');
			}
		});
    }
    $scope.clearCart = function(cartId){
    	console.log(cartId);
    	$http({
	        method: 'POST',
	        url: '/ElectronicStore/cart/removeAll',
	        data: $.param({
	        	cartId:parseInt(cartId)
	        	}),
	        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    }).then(function(response) {
			if(response.status == 200 ){
				$window.location.reload();
			}else {
				console.log('error');
			}

		});
    }
}).run(function($rootScope, $state, $http, $timeout, $interval,$cookieStore) {
	 var timer=$interval(function(){
		  var i=0;
		  i++;
		  $rootScope.thisUser = $cookieStore.get('user'); 
		  }, 1000);

	 $rootScope.checkoutBtn = function(total){
			$state.go('payment');
			$rootScope.totalPayment = total;
	}
	 if($cookieStore.get('user')){
		 var timer=$interval(function(){
			  var i=0;
			  i++;
		      $http.get('/ElectronicStore/getCart').success(function(response) {
	    		$rootScope.cartItems = response.cartitems;
		   		$rootScope.totalPayment = response.cart.totalPayment;
		   		$rootScope.cartId = response.cart.cartId;
		   		$rootScope.addedItem="Add";
				  for(var x in $rootScope.productsList){
				    for(var c in $rootScope.cartItems){
					   	if ($rootScope.cartItems[c].product.productId==$rootScope.productsList[x].productId){
					   		$rootScope.addedItem="Added";
						}			
					 }
				  }
		     }).error(function(response) { 
		    	 console.log("ERROR");
		     });
	     }, 1000);
	 }
});