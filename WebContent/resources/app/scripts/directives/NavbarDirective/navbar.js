angular.module('ElectronicStore.navbar',[]).directive('navbar', function($cookieStore){
	return{
		 restrict:'E',
		 scope:{},
		 controller:function($scope, $window, $http, $interval,$state){
			 $scope.logout = function() {
					$cookieStore.remove('user');
					$cookieStore.remove('rememberMe');
				}
			   $scope.adminlogout = function(){
					$cookieStore.remove('admin');
				    $state.go('adminlogin');
				}
			   var timer=$interval(function(){
					  var i=0;
					  i++;
			 	$scope.userCookie = $cookieStore.get('user');
			 	$scope.adminCookie = $cookieStore.get('admin');
				if($scope.userCookie){	
					$scope.authenticated=true;
					$scope.adminauthenticated=false;
					if($scope.userCookie.authority =="ROLE_CUSTOMER"){
						$scope.link = "#/customer";
					}else if($scope.userCookie.authority =="ROLE_ADMIN"){ 					
						$scope.link = "#/admin";
					}
				}else if($scope.adminCookie){	
					$scope.adminauthenticated=true;
					$scope.authenticated=false;
					} else{
						$scope.adminauthenticated=false;
						$scope.authenticated=false;
					}
			   }, 1000);
				 if($cookieStore.get('user')){
					 var timer=$interval(function(){
						  var i=0;
						  i++;
						  $http.get('/ElectronicStore/getCart').success(function(response) {
					    		$scope.cartItems = response.cartitems;
					    	});
					 }, 1000);
				 }
			},
		 templateUrl:'static/app/scripts/directives/NavbarDirective/navbar.html',
		 transclude:false
	}
});