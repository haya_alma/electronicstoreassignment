angular.module('ElectronicStore', [
    'ui.router',
    'ngCookies',
    'ngRoute',
    'ngSanitize',
    'ElectronicStore',
    'ElectronicStore.ShopCtrl',
    'ElectronicStore.LoginCtrl',
    'ElectronicStore.CustomerCtrl',
    'ElectronicStore.RegisterCtrl',
    'ElectronicStore.navbar',
    'ElectronicStore.rate',
    'ElectronicStore.CartCtrl',
    'ElectronicStore.PaymentCtrl',
    'ElectronicStore.AdminCtrl',
    ]).
  config(function ($stateProvider, $urlRouterProvider ) {	
	  
	  $urlRouterProvider.otherwise("/");
	  
	  $stateProvider
      .state('/', {
    	url: "/",
        templateUrl: 'static/app/views/shop.html',
        controller: 'ShopCtrl',
        controllerAs: 'shop'
      })
       .state('viewItem', {
    	url: "/viewItem",
        templateUrl: 'static/app/views/item.html',
        controller: 'ShopCtrl',
        controllerAs: 'shop'
      })
       .state('login', {
    	  url: "/login",
        templateUrl: 'static/app/views/login.html',
        controller: 'LoginCtrl',
        controllerAs: 'login'
      })
      .state('adminlogin', {
    	  url: "/adminlogin",
        templateUrl: 'static/app/views/adminLogin.html',
        controller: 'AdminCtrl',
        controllerAs: 'AdminCtrl'
      })
      .state('admin', {
    	  url: "/admin",
        templateUrl: 'static/app/views/admin.html',
        controller: 'AdminCtrl',
        controllerAs: 'AdminCtrl'
      })
      .state('stock', {
    	  url: "/stock",
        templateUrl: 'static/app/views/stock.html',
        controller: 'AdminCtrl',
        controllerAs: 'AdminCtrl'
      })
      .state('register', {
    	  url: "/register",
        templateUrl: 'static/app/views/register.html',
        controller: 'RegisterCtrl',
        controllerAs: 'register'
      })
       .state('customer', {
    	  url: "/customer",
        templateUrl: 'static/app/views/customer.html',
        controller: 'CustomerCtrl',
        controllerAs: 'customer'
      })
      .state('cart', {
    	  url: "/cart",
        templateUrl: 'static/app/views/cart.html',
        controller: 'CartCtrl',
        controllerAs: 'cart'
      })
	  .state('payment', {
		  url: "/payment",
		  templateUrl: 'static/app/views/payment.html',
		  controller: 'PaymentCtrl',
		  controllerAs: 'payment'
	  });
  });
