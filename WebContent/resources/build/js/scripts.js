angular.module('ElectronicStore', [
    'ui.router',
    'ngCookies',
    'ngRoute',
    'ngSanitize',
    'ElectronicStore',
    'ElectronicStore.ShopCtrl',
    'ElectronicStore.LoginCtrl',
    'ElectronicStore.CustomerCtrl',
    'ElectronicStore.RegisterCtrl',
    'ElectronicStore.navbar',
    'ElectronicStore.rate',
    'ElectronicStore.CartCtrl',
    'ElectronicStore.PaymentCtrl',
    'ElectronicStore.AdminCtrl',
    ]).
  config(function ($stateProvider, $urlRouterProvider ) {	
	  
	  $urlRouterProvider.otherwise("/");
	  
	  $stateProvider
      .state('/', {
    	url: "/",
        templateUrl: 'static/app/views/shop.html',
        controller: 'ShopCtrl',
        controllerAs: 'shop'
      })
       .state('viewItem', {
    	url: "/viewItem",
        templateUrl: 'static/app/views/item.html',
        controller: 'ShopCtrl',
        controllerAs: 'shop'
      })
       .state('login', {
    	  url: "/login",
        templateUrl: 'static/app/views/login.html',
        controller: 'LoginCtrl',
        controllerAs: 'login'
      })
      .state('adminlogin', {
    	  url: "/adminlogin",
        templateUrl: 'static/app/views/adminLogin.html',
        controller: 'AdminCtrl',
        controllerAs: 'AdminCtrl'
      })
      .state('admin', {
    	  url: "/admin",
        templateUrl: 'static/app/views/admin.html',
        controller: 'AdminCtrl',
        controllerAs: 'AdminCtrl'
      })
      .state('stock', {
    	  url: "/stock",
        templateUrl: 'static/app/views/stock.html',
        controller: 'AdminCtrl',
        controllerAs: 'AdminCtrl'
      })
      .state('register', {
    	  url: "/register",
        templateUrl: 'static/app/views/register.html',
        controller: 'RegisterCtrl',
        controllerAs: 'register'
      })
       .state('customer', {
    	  url: "/customer",
        templateUrl: 'static/app/views/customer.html',
        controller: 'CustomerCtrl',
        controllerAs: 'customer'
      })
      .state('cart', {
    	  url: "/cart",
        templateUrl: 'static/app/views/cart.html',
        controller: 'CartCtrl',
        controllerAs: 'cart'
      })
	  .state('payment', {
		  url: "/payment",
		  templateUrl: 'static/app/views/payment.html',
		  controller: 'PaymentCtrl',
		  controllerAs: 'payment'
	  });
  });

angular.module('ElectronicStore.AdminCtrl', []).controller('AdminCtrl', 
		function($scope, $rootScope, $http, $cookieStore, $cookies,  $state, $window, $sce, $interval) {
	$scope.adminLogin= function(){
		console.log($scope.adminUsername + " | " + $scope.adminPassword);
		$http({
	        method: 'POST',
	        url: '/ElectronicStore/adminlogin',
	        data: $.param({
	            username: $scope.adminUsername,
	            password: $scope.adminPassword
	            }),
	        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    }).then(function(response) {
			console.log(response);
			if(response.status == 200){
				$cookieStore.put('admin', response.data);
			    $state.go('admin');
			}	
		});
	}
	if(!$cookieStore.get('admin')){
		$state.go('adminlogin');
	}else{
		
		
		
	$scope.updateStock = function(product){
		console.log(product);
		$scope.item = product;
		var modal = document.getElementById("itemModal");
		modal.style.display = "block";
		
		 window.onclick = function (event) {
		        if (event.target == modal) {
		            modal.style.display = "none";
		        }
		     }
	}
	$scope.updateItem = function (productId, unitInStock, productPrice) {
		$http({
		        method: 'POST',
		        url: '/ElectronicStore/updateStock',
		        data: $.param({
		        	productId:productId,
		        	unitInStock: unitInStock, 
		        	productPrice: productPrice
		          }),
		        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    }).then(function (response) {
		    	if(response.status == 200){
		    		$state.go('stock');
		    		var modal = document.getElementById("itemModal");
		    		modal.style.display = "none";
		    	}
		    });
		}
	      
	$scope.thisAdmin=$cookieStore.get('admin');
	
	$http.get('/ElectronicStore/allOrders').then(function(response) {
		 console.log(response.data)
		 $scope.orders = response.data.orders;
		 $scope.items = response.data.items;
	 });
	 $http.get('/ElectronicStore/addressList').then(function(response) {
		 $scope.addressList = response.data;
	 });
	
	}
});

angular.module('ElectronicStore.CartCtrl', []).controller('CartCtrl', 
		function($scope, $scope, $http, $cookieStore, $cookies,  $state, $window, $sce, $interval) {  
	
	$scope.deleteItem = function(productId){
    	$http({
	        method: 'POST',
	        url: '/ElectronicStore/cart/remove',
	        data: $.param({
	        	productId: productId
	        	}),
	        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    }).then(function(response) {
			if(response.status == 200 ){
				$window.location.reload();
			}else {
				console.log('error');
			}
		});
    }
    $scope.clearCart = function(cartId){
    	console.log(cartId);
    	$http({
	        method: 'POST',
	        url: '/ElectronicStore/cart/removeAll',
	        data: $.param({
	        	cartId:parseInt(cartId)
	        	}),
	        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    }).then(function(response) {
			if(response.status == 200 ){
				$window.location.reload();
			}else {
				console.log('error');
			}

		});
    }
}).run(function($rootScope, $state, $http, $timeout, $interval,$cookieStore) {
	 var timer=$interval(function(){
		  var i=0;
		  i++;
		  $rootScope.thisUser = $cookieStore.get('user'); 
		  }, 1000);

	 $rootScope.checkoutBtn = function(total){
			$state.go('payment');
			$rootScope.totalPayment = total;
	}
	 if($cookieStore.get('user')){
		 var timer=$interval(function(){
			  var i=0;
			  i++;
		      $http.get('/ElectronicStore/getCart').success(function(response) {
	    		$rootScope.cartItems = response.cartitems;
		   		$rootScope.totalPayment = response.cart.totalPayment;
		   		$rootScope.cartId = response.cart.cartId;
		   		$rootScope.addedItem="Add";
				  for(var x in $rootScope.productsList){
				    for(var c in $rootScope.cartItems){
					   	if ($rootScope.cartItems[c].product.productId==$rootScope.productsList[x].productId){
					   		$rootScope.addedItem="Added";
						}			
					 }
				  }
		     }).error(function(response) { 
		    	 console.log("ERROR");
		     });
	     }, 1000);
	 }
});
angular.module('ElectronicStore.CustomerCtrl', []).controller('CustomerCtrl', 
		function($scope, $rootScope, $http, $cookieStore, $cookies,  $state, $window, $sce, $interval) {
		$scope.checkboxModel=true;
		 $scope.items = [];
	        $scope.add = function () {
	          $scope.items.push({ 
	             product: ""
	          });
	        };
			$http.get('/ElectronicStore/account').then(function(response) {
				if (true == $scope.checkboxModel){
					var now = new Date(), expiresValue = new Date(now.getFullYear()+1, now.getMonth(), now.getDate());
					$cookieStore.put('user', response.data,  {'expires' : expiresValue});
				}else if (false == $scope.checkboxModel){
					 var today = new Date();
					 var expiresValue = new Date(today);
					 expiresValue.setMinutes(today.getMinutes() + 60);
					$cookieStore.put('user', response.data, {'expires' : expiresValue});
				}
			});
		if('true' == angular.isUndefined($cookies.getObject('user'))){
			$state.go('login');
		} 
		 $scope.UpdateAccount = function () {
			$http({
		        method: 'POST',
		        url: '/ElectronicStore/updateAccountDetails',
		        data: $.param({
		        	id: $scope.id,
		            firstname: $scope.firstname,
		            lastname: $scope.lastname,
		            email: $scope.email,
		            descr: $scope.descr 
		        }),
		        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    });
			 modal.style.display = "none";
		 }
		 
		 $http.get('/ElectronicStore/userOrders').then(function(response) {
			 console.log(response.data);
			 $scope.userOrders = [];
			 for (var j=0; j < response.data.length; j++) {
					var dateStr = JSON.parse(response.data[j].date);  					        
					response.data[j].date = new Date(dateStr);
					$scope.userOrders.push(response.data[j]);
				}
		 });
		 $http.get('/ElectronicStore/allOrders').then(function(response) {
			 $scope.allOrders = [];
			 for (var o=0; o< response.data.items.length; o++) {
					var dateStr = JSON.parse(response.data.items[o].order.date);  					        
					response.data.items[o].order.date = new Date(dateStr);
					$scope.allOrders.push(response.data.items[o]);
				}
		 });
		 $http.get('/ElectronicStore/addressList').then(function(response) {
			 $scope.addressList = response.data;
		 });
});

angular.module('ElectronicStore.LoginCtrl', []).controller('LoginCtrl',
		function($scope, $rootScope, $http, $cookieStore, $state, $window) {

	$scope.LoginButton = function (){ 
			if($scope.username != null && $scope.password != null){
					    event.preventDefault();
					    $('form').fadeOut(500);
					    $('.wrapper').addClass('form-success');
			 }
		}
  })
.run(function($rootScope, $cookieStore, $state) {	
	$rootScope.checkBoxClicked = function( checkboxModel){
		$cookieStore.put('rememberMe', checkboxModel);
	}
	$rootScope.checkboxModel= $cookieStore.get('rememberMe');
	
	$rootScope.test="Testing this";
	if(!$cookieStore.get('user')){
		$state.go('login');
	}
		  	
});

angular.module('ElectronicStore.PaymentCtrl', [])
.controller('PaymentCtrl', function($scope,$http,$cookies, $window, $state) {

	$scope.billing = function(){
		$scope.paypal ='';		
	}
	
	$scope.delivery = function(){
		$scope.creditCard = '';
	}
	
	$scope.shopGo = function(){
		$state.go('products');
	}

	$scope.payPalSubmit= function(){
	    
    	$http({
	        method: 'POST',
	        url: '/ElectronicStore/paymentP',
	        data: $.param({
	        	totalPayment: parseInt($scope.totalPayment),
	        	selectedPayment: "Paypal",
				email: $scope.payPalEmail, 
				addr1:$scope.addr1,
				addr2:$scope.addr2,
				city:$scope.city,
				country:$scope.country
	        	}),
	        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    }).then(function(response) {
			if(response.status == 200 ){
				console.log(200);
			    $state.go('/');
			}	
		});
	}
	
	var stripe = Stripe('pk_test_OPz3O0SbCrzKE5QrON0QfA0r');
	var elements = stripe.elements();

	var card = elements.create('card', {
	  iconStyle: 'solid',
	  style: {
	    base: {
	      iconColor: '#8898AA',
	      color: 'white',
	      lineHeight: '36px',
	      fontWeight: 300,
	      fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
	      fontSize: '16px',

	      '::placeholder': {
	        color: '#8898AA',
	      },
	    },
	    invalid: {
	      iconColor: '#e85746',
	      color: '#e85746',
	    }
	  },
	  classes: {
	    focus: 'is-focused',
	    empty: 'is-empty',
	  },
	});
	card.mount('#card-element');

	var inputs = document.querySelectorAll('input.field');
	Array.prototype.forEach.call(inputs, function(input) {
	  input.addEventListener('focus', function() {
	    input.classList.add('is-focused');
	  });
	  input.addEventListener('blur', function() {
	    input.classList.remove('is-focused');
	  });
	  input.addEventListener('keyup', function() {
	    if (input.value.length === 0) {
	      input.classList.add('is-empty');
	    } else {
	      input.classList.remove('is-empty');
	    }
	  });
	});

	function setOutcome(result) {
	  var successElement = document.querySelector('.success');
	  var errorElement = document.querySelector('.error');
	  successElement.classList.remove('visible');
	  errorElement.classList.remove('visible');
	
	  if (result.token) {
	    successElement.querySelector('.token').textContent = result.token.id;
	    successElement.classList.add('visible');
	    
    	$http({
	        method: 'POST',
	        url: '/ElectronicStore/payment',
	        data: $.param({
	        	token: result.token.id,
	        	totalPayment: parseInt($scope.totalPayment),
	        	selectedPayment: "creditCard",
				email: $scope.email, 
				cardNumber: parseInt(result.token.card.last4), 
				carholdername: $scope.cardholdername,
				expireDate: $scope.expireDate,
				addr1:$scope.addr1,
				addr2:$scope.addr2,
				city:$scope.city,
				country:$scope.country
	        	}),
	        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    }).then(function(response) {
			if(response.status == 200 ){
				console.log(200);
			    $state.go('/');
			}	
		});
	    
	  } else if (result.error) {
	    errorElement.textContent = result.error.message;
	    errorElement.classList.add('visible');
	  }
	}

	card.on('change', function(event) {
	  setOutcome(event);
	});

	document.querySelector('form').addEventListener('submit', function(e) {
	  e.preventDefault();
	  var form = document.querySelector('form');
	  var extraDetails = {
	    name: form.querySelector('input[name=cardholder-name]').value,
	  };
	  stripe.createToken(card, extraDetails).then(setOutcome);
	});
});
angular.module('ElectronicStore.RegisterCtrl', [])
.controller('RegisterCtrl', function($scope,$http,$cookies, $window, $state) {

	
	$scope.Register = function () {
		if($scope.regUsername != null || $scope.regPassword != null || $scope.regEmail != null){
			$http({
		        method: 'POST',
		        url: '/ElectronicStore/createAccount',
		        data: $.param({
		            username: $scope.regUsername,
		            password: $scope.regPassword,
		            email: $scope.regEmail
		          }),
		        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    }).success(function (data, status, headers, config) {
		    	console.log('s >> ' + headers()['message']);
		    	if(headers()['message'] != null){
			    	$scope.message = headers()['message'];
		    	}else {
		    		$state.go('login');
		    	}
		    }).error(function (data, status, headers, config) {
		    	console.log('error response >> ' + headers()['message']);
		    });
		}else if($scope.regUsername == null || $scope.regPassword == null || $scope.regEmail == null){
			$scope.message = "Enter Your Details !";
		}
	}
	
	$(function() {
	    $("#confirmPassword").keyup(function() {
	        var password = $("#regPassword").val();
	        $("#divCheckPasswordMatch").html(password == $(this).val() ? "Passwords match." : "Passwords do not match!");
	    });

	});

  });

angular.module('ElectronicStore.ShopCtrl', []).controller('ShopCtrl',
		function($scope, $http, $cookieStore, $state, $window, $interval) {
	
	$scope.addComment = function(productId, comment){
		if(!angular.isUndefined($scope.thisUser)){
   	 	$http({
		        method: 'POST',
		        url: '/ElectronicStore/addComment',
		        data: $.param({
		        	productId: parseInt(productId),
		        	commentText: comment
		        	}),
		        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    }).then(function(response) {		  
		    	 document.getElementById("commentForm").reset();
		    	 if(response.status == 200 ){
						$window.location.reload();
					}
		    });
		 }else if(angular.isUndefined($scope.thisUser)){
	  			$state.go('login');
		  }
	   }
	$scope.deleteComment = function(commentId){
		if(!angular.isUndefined($scope.thisUser)){
   	 	$http({
		        method: 'POST',
		        url: '/ElectronicStore/removeComment',
		        data: $.param({
		        	commentId: commentId
		        	}),
		        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    });
		  }else if(angular.isUndefined($scope.thisUser)){
	  			$state.go('login');
		  }
		}
		
		$scope.viewItem = function (product) {
			$state.go("viewItem");
			$scope.product =product
	        $window.sessionStorage.setItem('product', JSON.stringify(product));
		}	
		if($scope.product == null){
			var temp = sessionStorage.getItem('product');
			$scope.product = $.parseJSON(temp);	
		}
		
		$http.get('/ElectronicStore/commentsList').
	    success(function(response) {
	    	$scope.commentsList = response;
	    });
		
		$http.get('/ElectronicStore/ratesList').
	    then(function(response) {
	    	$scope.ratesList = response.data;
	    	$scope.reviewAverageList=[];	
			
	    	$scope.dataReviews = $scope.ratesList, grouped = Object.create(null);
	    	$scope.dataReviews.forEach(function (a) {
			    grouped[a.product.productId] = grouped[a.product.productId] || [];
			    grouped[a.product.productId].push(a);
			});
			 for(var i in grouped) { 
				 	var ratingTotal=0; 
					for(var x = 0; x< grouped[i].length;  x++) {
					     ratingTotal += grouped[i][x].rating; 
					 }
	    			 $scope.Total = grouped[i].length * 10;
	    			 $scope.ratingAverage = (ratingTotal / $scope.Total) * 10;
	    			 $scope.reviewAverageList.push({id : i, rating : $scope.ratingAverage.toFixed(2) });
			 }	
	    });
	
		
		  $scope.getCategory = function (category) {
			  console.log(category)
			  $http({
		        method: 'POST',
		        url: '/ElectronicStore/getCategory',
		        data: $.param({
		        	category: category
		        	}),
		        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    }).then(function(response) {
		    	$scope.productsList = response.data.category;
		    });
		};
		  $scope.getBrand = function (manufacturer) {
			  $http({
		        method: 'POST',
		        url: '/ElectronicStore/getManufacturer',
		        data: $.param({
		        	manufacturer: manufacturer
		        	}),
		        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    }).then(function(response) {
		    	$scope.productsList = response.data.manufacturer;
		    });
		};
		
	    $scope.rating1 = 0;

	    $scope.click1 = function (param, productId) {
	        console.log('Click || ' + param );
	    	$http({
		        method: 'POST',
		        url: '/ElectronicStore/addRate',
		        data: $.param({
		        	productId: parseInt(productId),
		        	rating: param
		        	}),
		        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    });
	    }
			$scope.addItem = function (productId, quantity) {	
				console.log(productId, quantity);
				 if(!angular.isUndefined($scope.thisUser)){
						$scope.itemAdded = false;
						for (var j=0; j < $scope.cartItems.length; j++) {
							if(productId == $scope.cartItems[j].product.productId ){
								$scope.itemAdded = true;
							}
						}
						if( false == $scope.itemAdded ){ 
								$http({
						        method: 'POST',
						        url: '/ElectronicStore/cart/add',
						        data: $.param({
						        	productId: productId,
						        	quantity: parseInt($scope.quantity)
						        	}),
						        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
						    }).then(function(response) {
								if(response.status == 200 ){
									var timer=$interval(function(){
										var x=0;
										x++;
										$window.onload = function what(){
										var addButton=	document.getElementById("addItemBtn");
										 addButton.innerHTML="Added";
											};
								    },300);
								}else {
									console.log('error');
								}
							});
						}
						
				 }else if(angular.isUndefined($scope.thisUser)){
			  			$state.go('login');
				  }
			}
			
  }).run(function($rootScope, $state, $http, $timeout, $interval,$cookieStore) {
	  $http.get('/ElectronicStore/products').
	    success(function(response) {
	    	$rootScope.productsList = response;
	    	console.log($rootScope.productsList)
	    });
	  
	  $rootScope.propertyName = 'productName';
	  $rootScope.reverse = true;

	  $rootScope.sortBy = function(propertyName) {
		  $rootScope.reverse = ($rootScope.propertyName === propertyName) ? !$rootScope.reverse : false;
		  $rootScope.propertyName = propertyName;
	  };
  });
angular.module('ElectronicStore.navbar',[]).directive('navbar', function($cookieStore){
	return{
		 restrict:'E',
		 scope:{},
		 controller:function($scope, $window, $http, $interval,$state){
			 $scope.logout = function() {
					$cookieStore.remove('user');
					$cookieStore.remove('rememberMe');
				}
			   $scope.adminlogout = function(){
					$cookieStore.remove('admin');
				    $state.go('adminlogin');
				}
			   var timer=$interval(function(){
					  var i=0;
					  i++;
			 	$scope.userCookie = $cookieStore.get('user');
			 	$scope.adminCookie = $cookieStore.get('admin');
				if($scope.userCookie){	
					$scope.authenticated=true;
					$scope.adminauthenticated=false;
					if($scope.userCookie.authority =="ROLE_CUSTOMER"){
						$scope.link = "#/customer";
					}else if($scope.userCookie.authority =="ROLE_ADMIN"){ 					
						$scope.link = "#/admin";
					}
				}else if($scope.adminCookie){	
					$scope.adminauthenticated=true;
					$scope.authenticated=false;
					} else{
						$scope.adminauthenticated=false;
						$scope.authenticated=false;
					}
			   }, 1000);
				 if($cookieStore.get('user')){
					 var timer=$interval(function(){
						  var i=0;
						  i++;
						  $http.get('/ElectronicStore/getCart').success(function(response) {
					    		$scope.cartItems = response.cartitems;
					    	});
					 }, 1000);
				 }
			},
		 templateUrl:'static/app/scripts/directives/NavbarDirective/navbar.html',
		 transclude:false
	}
});
angular.module('ElectronicStore.rate',[]).directive('rate', function($cookieStore){
    return {
        scope: {
            rating: '=',
            maxRating: '@',
            readOnly: '@',
            click: "&",
            mouseHover: "&",
            mouseLeave: "&"
        },
        restrict: 'EA',
        templateUrl: 'static/app/scripts/directives/RateDirective/rate.html',
        transclude: false,
        compile: function (element, attrs) {
            if (!attrs.maxRating || (Number(attrs.maxRating) <= 0)) {
                attrs.maxRating = '5';
            };
        },
        controller: function ($scope, $element, $attrs) {
            $scope.maxRatings = [];

            for (var i = 1; i <= $scope.maxRating; i++) {
                $scope.maxRatings.push({});
            };

            $scope._rating = $scope.rating;
			
			$scope.isolatedClick = function (param) {
				if ($scope.readOnly == 'true') return;
				$scope.rating = $scope._rating = param;
				$scope.hoverValue = 0;
				$scope.click({
					param: param
				});
			};
        }
    };
});
